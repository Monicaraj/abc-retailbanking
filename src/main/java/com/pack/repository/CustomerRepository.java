package com.pack.repository;


import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.pack.modal.*;

public interface CustomerRepository extends CrudRepository<Customer, Integer>{

	List<Customer> findAll();

	

	Customer findById(int id);

	
}
