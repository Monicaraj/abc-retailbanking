package com.pack.repository;


import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.pack.modal.TransDetailStatement;

public interface TransRepository extends CrudRepository<TransDetailStatement, Integer> {

	List<TransDetailStatement> findByCustId(Integer custId );
	
}
