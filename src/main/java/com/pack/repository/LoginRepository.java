package com.pack.repository;


import org.springframework.data.repository.CrudRepository;

import com.pack.modal.*;

public interface LoginRepository extends CrudRepository<Login, Integer>{

	Login findByuserName(String userName);

	
}
