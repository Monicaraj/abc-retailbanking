package com.pack.repository;

import org.springframework.data.repository.CrudRepository;

import com.pack.modal.Account;

public interface AccountRepository extends CrudRepository<Account, Integer> {

	
	Account findByCustId(Integer id);

}
