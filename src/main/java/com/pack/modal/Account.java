package com.pack.modal;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Account {
	int custId;
	@Id
	
	int accNum;
	String accType;
	double balance;
	String date;
	public int getCustId() {
		return custId;
	}
	public void setCustId(int custId) {
		this.custId = custId;
	}
	public int getAccNum() {
		return accNum;
	}
	public void setAccNum(int accNum) {
		this.accNum = accNum;
	}
	public String getAccType() {
		return accType;
	}
	public void setAccType(String accType) {
		this.accType = accType;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Account(int custId, int accNum, String accType, double balance, String date) {
		super();
		this.custId = custId;
		this.accNum = accNum;
		this.accType = accType;
		this.balance = balance;
		this.date = date;
	}
	public Account() {
		super();
	}
	
	
	

}
