package com.pack.modal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class TransDetailStatement {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int transId;
	private String transDate;
	private String transRemarks;
	private String transType;
	private double balanceOnDate;
	private double transAmnt;
	private int fromAccNo;
	private int toAccNo;
	private String transStatus;
	private int custId;
	
	
	public TransDetailStatement(int transId, String transDate, String transRemarks, String transType,
			double balanceOnDate, double transAmnt, int fromAccNo, int toAccNo, String transStatus, int custId) {
		super();
		this.transId = transId;
		this.transDate = transDate;
		this.transRemarks = transRemarks;
		this.transType = transType;
		this.balanceOnDate = balanceOnDate;
		this.transAmnt = transAmnt;
		this.fromAccNo = fromAccNo;
		this.toAccNo = toAccNo;
		this.transStatus = transStatus;
		this.custId = custId;
	}
	public int getCustId() {
		return custId;
	}
	public void setCustId(int custId) {
		this.custId = custId;
	}
	public TransDetailStatement() {
		super();
	}
	
	public int getTransId() {
		return transId;
	}
	public void setTransId(int transId) {
		this.transId = transId;
	}
	public String getTransDate() {
		return transDate;
	}
	public void setTransDate(String transDate) {
		this.transDate = transDate;
	}
	public String getTransRemarks() {
		return transRemarks;
	}
	public void setTransRemarks(String transRemarks) {
		this.transRemarks = transRemarks;
	}
	public String getTransType() {
		return transType;
	}
	public void setTransType(String transType) {
		this.transType = transType;
	}
	public double getBalanceOnDate() {
		return balanceOnDate;
	}
	public void setBalanceOnDate(double balanceOnDate) {
		this.balanceOnDate = balanceOnDate;
	}
	public double getTransAmnt() {
		return transAmnt;
	}
	public void setTransAmnt(double transAmnt) {
		this.transAmnt = transAmnt;
	}
	public int getFromAccNo() {
		return fromAccNo;
	}
	public void setFromAccNo(int fromAccNo) {
		this.fromAccNo = fromAccNo;
	}
	public int getToAccNo() {
		return toAccNo;
	}
	public void setToAccNo(int toAccNo) {
		this.toAccNo = toAccNo;
	}
	public String getTransStatus() {
		return transStatus;
	}
	public void setTransStatus(String transStatus) {
		this.transStatus = transStatus;
	}
	
	
	
}
