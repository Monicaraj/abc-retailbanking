package com.pack.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.pack.modal.Account;
import com.pack.modal.Customer;
import com.pack.modal.Login;
import com.pack.repository.AccountRepository;
import com.pack.repository.CustomerRepository;
import com.pack.repository.LoginRepository;

@RestController
public class RegistrationController {

	
	@Autowired
	LoginRepository loginRepository;
	@Autowired
	AccountRepository accountRepository;
	@Autowired
	CustomerRepository customerRepository;
	@CrossOrigin
	@RequestMapping(value="checkregister",method=RequestMethod.POST)
	public String checkLogin(@RequestBody Customer customer)
		{
		 
			try {
				customerRepository.save(customer);
				Random rand = new Random(); 
				Integer accNum = rand.nextInt(999999)+1000000;
				Date date = Calendar.getInstance().getTime();  
				DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");  
				String strDate = dateFormat.format(date);  
				double balance=0.0;
				String password =Integer.toString(rand.nextInt(999999)+1000000);
				Login l = new Login(customer.getId(), customer.getName(), password, "user");				
				loginRepository.save(l);
				Account account=new  Account(customer.getId(),accNum,customer.getAccType(),balance,strDate);
				accountRepository.save(account);
				return password;
					}
			catch (Exception e) {
				
				return null;
				}
		}
	
	@CrossOrigin
	@RequestMapping(value="viewCustomers",method=RequestMethod.GET)
	public List<Customer> viewAccounts()
		{
		 
			try {
				
				return customerRepository.findAll();
				 
					}
			catch (Exception e) {
				return null;
				}
		}
	@CrossOrigin
	@RequestMapping(value="/editCustomer",method=RequestMethod.POST)
	public Customer updateData(@RequestBody Customer customer)
	{	
         Customer customer1=customerRepository.findById(customer.getId());
        
         customer1.setAddress(customer.getAddress());
         customer1.setMobno(customer.getMobno());
         	customerRepository.save(customer1);
         	return customer1;
	}
		
	 
}