package com.pack.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pack.modal.Account;
import com.pack.repository.AccountRepository;
import com.pack.repository.CustomerRepository;
import com.pack.repository.LoginRepository;

@RestController
public class SearchController {
	@Autowired
	LoginRepository loginRepository;
	@Autowired
	AccountRepository accountRepository;
	@Autowired
	CustomerRepository customerRepository;
	
	@RequestMapping(value="searchByNo",method=RequestMethod.POST)
	public Account searchByAccountNo(@RequestBody Account account)
	{
	
		return accountRepository.findOne(account.getAccNum());
	}
}
