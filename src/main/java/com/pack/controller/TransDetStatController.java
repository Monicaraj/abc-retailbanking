package com.pack.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.pack.modal.TransDetailStatement;
import com.pack.repository.TransRepository;

@RestController
public class TransDetStatController {
	
	@Autowired
	TransRepository transRepository;
	@CrossOrigin
	@RequestMapping(value="checkTrans/{id}",method=RequestMethod.GET)
	public  List<TransDetailStatement> checkLogin(@PathVariable int id)
		{
		 
			try {
				return transRepository.findByCustId(id);
			}
			catch (Exception e) {
				return new ArrayList<TransDetailStatement>();
				}
			
		}

}
