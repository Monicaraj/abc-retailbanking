package com.pack.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.pack.modal.Account;
import com.pack.repository.AccountRepository;

@RestController
public class AccountController {
	@Autowired
	AccountRepository accountRepository;
	@CrossOrigin
    @RequestMapping(value="/viewAccount/{id}",method=RequestMethod.GET)
    public Account viewAccount(@PathVariable int id)
    {
       	return accountRepository.findByCustId(id);
    }
}
