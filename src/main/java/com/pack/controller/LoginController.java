package com.pack.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.pack.SpringBootApp;
import com.pack.modal.*;
import com.pack.repository.CustomerRepository;
import com.pack.repository.LoginRepository;


@RestController
public class LoginController {

	private static final Logger logger = LoggerFactory.getLogger(SpringBootApp.class);
	
	@Autowired
	LoginRepository loginRepository;
	@Autowired
	CustomerRepository customerRepository;
	@CrossOrigin 
	@RequestMapping(value="checkLogin",method=RequestMethod.POST)
	public String checkLogin(@RequestBody Login login)
		{
			try {
				
			Login	login1 = loginRepository.findByuserName(login.getUserName());
					
			if(login1.getPassword().equals(login.getPassword()))
			{
				
				return "Success";
			}
			else
				return "";
			}
			catch (Exception e) {
				logger.warn("ERROR");	
				return "";
				}
		}
	
			@RequestMapping(value = "/testlogin", method = RequestMethod.GET)
			public Login firstPage() {
			
			Login emp = new Login();
			emp.setUserName("Abhi");
			emp.setPassword("admin");
			emp.setRole("admin");
			return emp;
			}
			}

